<!--HTML TYPE--->

<!--1/2 5 kopi Bangka 5 Jakarta Selatan 12/19/2018 01:22-->
<!--Create By ManingCorp-->

<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
    <link rel="stylesheet" type="text/css" href="assets/css/responsive_style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css');?>">
    <link href="https://fonts.googleapis.com/css?family=Raleway|Staatliches" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url();?>assets/image/logo.png"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/custom.js">
    </script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js">
    </script>
    <title><?php echo COMPANY_NAME;?></title>
    <style>
    .color-white {
        color: #fff;
    }
    .color-black {
        color:#000;
    }
    </style>
  </head>
  <body>

    <!--open menu navbar for pc-->
    <header class="" id="menu-for-PC">
      <nav class="navbar navbar-default pos-navbar color-white background-transparent" id="menus-nav">
        <div class="pos-text-menus container-fluid">
          <div class="navbar-header pos-nav-name">
            <a class="navbar-brand" href="<?php echo base_url();?>"><?php echo COMPANY_NAME;?></a>
          </div>
          <ul class="nav navbar-nav pos-navbar-right">
            <li><a id="menu-text-a" class="color-white" href="#our-services">Our Services</a></li>
            <li><a id="menu-text-a2" class="color-white" href="#our-work">Our Works</a></li>
            <li><a id="menu-text-a3" class="color-white" href="#"><span class="fa fa-phone"></span>+6221 756 82 152</a></li>
          </ul>
        </div>
      </nav>
        <div class="pos-float-btn">
            <ul>
                <li><a data-toggle="modal" data-target="#ModalClintArea" href="#" id="float-btn" class="float-bnt-custom-color-white">Client Area <span class="fa fa-chevron-right cr-color-white" id="ciscle-01"></span></a></li>
                <li><a data-toggle="modal" data-target="#ModalWorkingArea" href="#" id="float-btn2" class="float-bnt-custom-color-white">Working Area <span class="fa fa-chevron-right cr-color-white" id="ciscle-02"></span></a></li>
            </ul>
        </div>
    </header>
    <?php $this->load->view($content);?>
    <div class="pos-btn-to-top">
      <a></>
    </div>
</body>
</html>
