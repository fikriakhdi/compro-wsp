<!--close menu navbar for pc-->
<head>
  <style>
  .navbar-default .navbar-nav>li>a {
    color:#fff;
  }
  .color-width {
      color:#fff!important;
  }
  .color-black {
      color:#000!important;
  }

  </style>
</head>
<a href="#" id="scroll" style="display: none;"><span></span></a>
<div id="content-web">
  <!--open content banner home-->
  <section class="container banner-style" id="banner-home">
    <div class="banner-content" style="background-image:url(<?php echo base_url('assets/image/banner/banner01.jpg');?>);">
      <div class="pos-text-banner">
        <p class="text-desc">Kamu memerlukan partner yang dapat menjawab semua masalah dalam dunia digital yang bergerak semakin cepat dan massive sekarang ini, karena...</p>
        <h1 class="firs-title-font">We Solve Problems Digitaly</h1>
          <div class="pos-button-banner">
              <a class="btn-banner" href="#">About Us <span class="fa fa-chevron-right"></span></a>
          </div>
      </div>
      <div class="pos-button-animate-scroll-to-explorer">
        <a></a>
      </div>
    </div>
  </section>
  <!--close content banner home-->

  <!--open content our services-->
  <section class="container" id="our-services">
    <div class="row">
      <div class="title-1-pos col-md-12">
        <div class="col-md-12 right-title">
          <h3>Our Services</h3>
        </div>
      </div>

      <div class="col-md-12 pos-internet-ofting">
        <div class="col-md-6 pos-text-desc-2 col-md-6">
        <div class="title-2-pos">
          <h4>Internet of Things <br> Development</h4>
        </div>
        <div class="pos-text-2">
          <p>
            Website menjadi sebuah keharusan bagi setiap pelaku bisnis sekarang ini. Melalui website customer anda akan dengan mudah menemukan apa yang mereka butuhkan, memberikan pertimbangan, mempersuasi, hingga bertransaksi. Jangan anggap sepele sebuah website. Website yang baik akan memeberikan dampak yang optimal dan memberikan kepercayaan. Kami berkomitmen untuk memberikan pelayanan terbaik untuk etalase digital Anda. Tampilan yang cantik, fungsionalitas tinggi dan dengan performa yang paling optimal. Nikmati juga maintenance dari kami selamanya.
          </p>
        </div>
        <div class="pos-button-all-suctom">
          <a class="btn-all-custom" href="<?php echo base_url('why_iot');?>">View More <span class="fa fa-chevron-right"></span></a>
        </div>
      </div>
        <div class="content-left-services col-md-6">
        <div class="pos-screen">
          <img class="size-img-prame-our-services " src="<?php echo base_url('assets/image/IoT website-01.png');?>">
        </div>
      </div>
      </div>

      <div class="col-md-12 pos-web-dev">
        <div class="content-left-services col-md-6">
        <div class="pos-screen">
          <img class="size-img-prame-our-services " src="<?php echo base_url('assets/image/web-dev-01.png');?>">
        </div>
      </div>
        <div class="pos-text-desc-2 col-md-6">
        <div class="title-2-pos">
          <h4>Website <br> Development</h4>
        </div>
        <div class="pos-text-2">
          <p>
            Website menjadi sebuah keharusan bagi setiap pelaku bisnis sekarang ini. Melalui website customer anda akan dengan mudah menemukan apa yang mereka butuhkan, memberikan pertimbangan, mempersuasi, hingga bertransaksi. Jangan anggap sepele sebuah website. Website yang baik akan memeberikan dampak yang optimal dan memberikan kepercayaan. Kami berkomitmen untuk memberikan pelayanan terbaik untuk etalase digital Anda. Tampilan yang cantik, fungsionalitas tinggi dan dengan performa yang paling optimal. Nikmati juga maintenance dari kami selamanya.
          </p>
        </div>
        <div class="pos-button-all-suctom">
          <a class="btn-all-custom" href="<?php echo base_url('why_website');?>">View More <span class="fa fa-chevron-right"></span></a>
        </div>
      </div>
    </div>
    </div>
  </section>

  <section class="container" id="our-services">
    <div class="row">
      <div class="col-md-12 pos-web-dev">
        <div class="content-left-services col-md-6 pos-text-desc-2">
          <div class="title-2-pos">
            <h4>Mobile Apps <br> Development</h4>
          </div>
          <div class="pos-text-2">
            <p>
              Miliki relasi tanpa batas ruang dan waktu dengan customer Anda. Hal itu hanya bisa dilakukan apabila Anda memiliki sebuah mobile apps yang selalu dikantongi oleh customer Anda. Berikan solusi digital yang terintegrasi bagi mereka, biarkan mereka mendapatkan penggalaman seutuhnya dari brand Anda, berikan perhatian-perhatian kecil melalui push notofikasi yang hanya bisa dilakukan pada mobile apps.
            </p>
          </div>
          <div class="pos-button-all-suctom">
            <a class="btn-all-custom" href="<?php echo base_url('why_mobile');?>">View More <span class="fa fa-chevron-right"></span></a>
          </div>
        </div>
        <div class="col-md-6">
          <div class="pos-screen">
              <div id="scren-lyer-con">
                  <img src="<?php echo base_url('assets/image/mob-dev-01.png');?>">

                  <!-- <div id="img11" class="img-lyr-41">
                      <img src="<?php echo base_url('assets/image/banner/WSP-Web-Images-04.png');?>">
                  </div>
                  <div id="img21" class="img-lyr-31">
                      <img src="<?php echo base_url('assets/image/banner/WSP-Web-Images-03.png');?>">
                  </div>
                  <div id="img31" class="img-lyr-21">
                      <img src="<?php echo base_url('assets/image/banner/WSP-Web-Images-02.png');?>">
                  </div>
                  <div id="img41" class="img-lyr-11">
                      <img src="<?php echo base_url('assets/image/banner/WSP-web-dev-01.png');?>">
                  </div> -->
              </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="container" id="Enterprise-services">
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-6 content-left-services">
          <div class="pos-screen">
            <img class="size-img-prame-our-services " src="<?php echo base_url('assets/image/ess-01.png');?>">
          </div>
        </div>
        <div class="col-md-6 pos-text-desc-2">
          <div class="title-2-pos">
            <h4>Enterprise <br> Solution System</h4>
          </div>
          <div class="pos-text-2">
            <p>
              Solusi bagi semua keperluan komputasi yang sistematis untuk bisnis Anda. Tingkatkan performa dan efisiensi bisnis melalui ESS dari kami . Miliki lah system yang saling terintegrasi, handal dan real time untuk setiap kegiatan perusahaan Anda. Dari mulai sistem POS, Absensi, Project Task Management, hingga inventori serahkan kepada kami dan Anda dapat fokus kepada kegiatan bisnis utama Anda. Track semua kegiatan bisnis Anda dalam satu platform real time dengan kecepatan maksimal.
            </p>
          </div>
          <div class="pos-button-all-suctom">
            <a class="btn-all-custom" href="#">View More <span class="fa fa-chevron-right"></span></a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--close content our services-->

  <!--open content why choose us-->
  <section class="container" id="why-choose-us">
    <div class="row">
      <div class="col-md-12 po-1200-size">
        <div class="title-1-pos-2">
          <h2 class="title-for-pc">Why Choose Us</h2>
          <h3 class="title-for-mobile">Why Choose Use</h3>
        </div>
        <div class="pos-cont-why-choose-us">
        <div class="pos-cont-why-choose-us-01">
          <div class="content-col3">
            <div class="head">
              <span>01</span>
            </div>
            <div class="body">
              <h3>Text header 1</h3>
            </div>
            <div class="foot">
              <p class="text-desc-choose">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
            </div>
          </div>
        </div>
        <div class="pos-cont-why-choose-us-01">
          <div class="content-col3">
            <div class="head">
              <span>02</span>
            </div>
            <div class="body">
              <h3>Text header 2</h3>
            </div>
            <div class="foot">
              <p class="text-desc-choose">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
            </div>
          </div>
        </div>

        <div class="pos-cont-why-choose-us-01">
          <div class="content-col3">
            <div class="head">
              <span>03</span>
            </div>
            <div class="body">
              <h3>Text header 3</h3>
            </div>
            <div class="foot">
              <p class="text-desc-choose">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
            </div>
          </div>
        </div>

        <div class="pos-cont-why-choose-us-01">
          <div class="content-col3">
            <div class="head">
              <span>04</span>
            </div>
            <div class="body">
              <h3>Text header 4</h3>
            </div>
            <div class="foot">
              <p class="text-desc-choose">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
            </div>
          </div>
        </div>
        <div class="pos-cont-why-choose-us-01">
          <div class="content-col3">
            <div class="head">
              <span>05</span>
            </div>
            <div class="body">
              <h3>Text header 5</h3>
            </div>
            <div class="foot">
              <p class="text-desc-choose">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
            </div>
          </div>
        </div>

        <div class="pos-cont-why-choose-us-01">
          <div class="content-col3">
            <div class="head">
              <span>06</span>
            </div>
            <div class="body">
              <h3>Text header 6</h3>
            </div>
            <div class="foot">
              <p class="text-desc-choose">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
  </section>
  <!--close content why choose us-->

  <!--open content our work-->
  <section class="container" class="" id="our-work">
    <div class="row">
      <div class="col-md-12 po-1200-size">
        <div class="title-1-pos-2 title-our-works">
          <h2 class="title-for-pc1">Our Works</h2>
          <h3 class="title-for-mobile">Our Works</h3>
        </div>
        <div id="myCarousel" class="carousel slide pos-caro-home" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
          </ol>

          <!-- Wrapper for slides -->
          <div class="carousel-inner">
            <div class="item active">
              <div class="content-crousel">
                  <div class="left">
                    <img class="size-img-prame-our-services for-mob-im" src="<?php echo base_url('assets/image/Mob-aps-works-01.png');?>">
                  </div>
              </div>
                <div class="content-crousel">
                    <div class="right">
                      <div class="title-2-pos">
                        <h4>Website <br> Development</h4>
                      </div>
                      <div class="pos-text-2">
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                        </p>
                      </div>
                      <div class="pos-button-all-suctom">
                        <a class="btn-all-custom" href="jejaring-apps.html">View More <span class="fa fa-chevron-right"></span></a>
                      </div>
                    </div>
                </div>
              </div>

              <div class="item">
              <div class="content-crousel">
                  <div class="left">
                  <img class="size-img-prame-our-services " src="<?php echo base_url('assets/image/motzint-web-01.png');?>">
                  </div>
              </div>
                <div class="content-crousel">
                    <div class="right">
                      <div class="title-2-pos">
                        <h4>Website <br> Development</h4>
                      </div>
                      <div class="pos-text-2">
                        <p >
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                        </p>
                      </div>
                      <div class="pos-button-all-suctom">
                        <a class="btn-all-custom" href="jejaring-apps.html">View More <span class="fa fa-chevron-right"></span></a>
                      </div>
                    </div>
                </div>
              </div>

              <div class="item">
              <div class="content-crousel">
                  <div class="left">
                    <img class="size-img-prame-our-services " src="<?php echo base_url('assets/image/Our-works-1-01.png');?>">
                  </div>
              </div>
                <div class="content-crousel">
                    <div class="right">
                      <div class="title-2-pos">
                        <h4>Website <br> Development</h4>
                      </div>
                      <div class="pos-text-2">
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                        </p>
                      </div>
                      <div class="pos-button-all-suctom">
                        <a class="btn-all-custom" href="jejaring-apps.html">View More <span class="fa fa-chevron-right"></span></a>
                      </div>
                    </div>
                </div>
              </div>

          </div>

          <!-- Left and right controls -->
          <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="fa fa-chevron-left"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="fa fa-chevron-right"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
    </div>
  </section>
  <!--close content our work-->

  <section class="container footer-content">
    <div class="row">
      <div class="col-md-12 po-1200-size">
        <div id="content-footer">
        <div class="foot-header">
            <h3>Hello WSP!</h3>
        </div>
        <div class="foot-body col-md-12">
            <div class="left col-md-6">
                <div class="foot-title">
                    <h4><a href="mailto:hello@wesolveproblems.id?Subject=hello"><span class="fa fa-envelope-o"></span><i  class="gradien-color-title-font">hello@wesolveproblems.id</i></a></h4>
                </div>
                <div class="body">
                    <ul>
                        <li><a href="https://bit.ly/2D9IbIv" target="_blank"><span class="fa fa-map-marker"></span> Perumahan Serpong Garden 2, Cluster Greenland,  Blok A13/5, Cisauk,  Tangerang, Banten, Indonesia</a></li>
                        <li><a href="tel:622175682152"><span class="fa fa-phone"></span>+6221 756 82 152</a></li>
                    </ul>
                </div>
            </div>
            <div class="right col-md-6">
                <div class="post-form">
                    <form id="" method="" action="">
                        <div class="form-group">
                            <label>Name</label>
                            <input class="text-input" type="text" name="name" id="name">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input class="text-input" type="text" name="email" id="email">
                        </div>
                        <div class="form-group">
                            <label>Message</label>
                            <textarea class="text-input" id="message" name="message" rows="3"></textarea>
                        </div>
                        <div class="btn-submit-pos">
                            <button type="submit" class="btn-submit">Send<span class="fa fa-chevron-right"></span></button>
                        </div>
                    </form>
                </div>
            </div>
        </div></div>
      </div>
    </div>
  </section>
  <footer>
    <div class="con-foot-01">
      <div class="right-cont-foot">
        <ul>
          <li>
            <a href="#">
              <img src="<?php echo base_url('assets/image/banner/logo-putih-01.png');?>">
            </a>
          </li>
        </ul>
      </div>
      <div class="center-cont-foot">
        <ul>
          <li>
            <a href="#">About Us</a>
          </li>
          <li>
            <a href="#">Our Services</a>
          </li>
          <li>
            <a href="#">Our Clients</a>
          </li>
          <li>
            <a href="#">Our Projects</a>
          </li>
        </ul>
      </div>
      <div class="left-cont-foot">
        <div class="title-foot">
          <h5>Our Channel</h5>
        </div>
        <ul>
          <li>
            <a href="#"><span class="fa fa-linkedin"></span></a>
          </li>
          <li>
            <a href="#"><span class="fa fa-whatsapp"></span></a>
          </li>
          <li>
            <a href="#"><span class="fa fa-instagram"></span></a>
          </li>
          <li>
            <a href="#"><span class="fa fa-facebook"></span></a>
          </li>
        </ul>
      </div>
    </div>
   </footer>
</div>

<div class="window" id="content-mobile">
  <div class="header">
    <div class="burger-container">
      <div id="burger">
        <div class="bar topBar"></div>
        <div class="bar btmBar"></div>
      </div>
    </div>
    <div class="icon icon-apple"></div>
    <div class="pos-icon-nav"><a href="<?php echo base_url('home');?>"><img src="<?php echo base_url('assets/image/banner/logo-putih-01.png');?>"></a></div>
    <ul class="menu">
      <li class="menu-item"><a href="#">Our Services</a></li>
      <li class="menu-item"><a href="#">Our Works</a></li>
      <li class="menu-item"><a href="#">Client Area</a></li>
      <li class="menu-item"><a href="#">Working Area</a></li>
    </ul>
    <!-- <div class="shop icon icon-bag"></div> -->
  </div>
  <div class="content">
    <!--open content banner home-->
    <section class="container banner-style" id="banner-home">
      <div class="banner-content" style="background-image:url(<?php echo base_url('assets/image/banner/banner01.jpg');?>);">
        <div class="pos-text-banner">
          <p class="text-desc">Kamu memerlukan partner yang dapat menjawab semua masalah dalam dunia digital yang bergerak semakin cepat dan massive sekarang ini, karena...</p>
          <h1 class="firs-title-font">We Solve Problems Digitaly</h1>
            <div class="pos-button-banner">
                <a class="btn-banner" href="#">About Us <span class="fa fa-chevron-right"></span></a>
            </div>
        </div>
        <div class="pos-button-animate-scroll-to-explorer">
          <a></a>
        </div>
      </div>
    </section>
    <!--close content banner home-->

    <!--open content our services-->
    <section class="container" id="our-services">
      <div class="row">
        <div class="title-1-pos col-md-12">
          <div class="col-md-12 right-title">
            <h3>Our Services</h3>
          </div>
        </div>

        <div class="col-md-12 pos-internet-ofting">
          <div class="content-left-services col-md-6">
          <div class="pos-screen">
            <img class="size-img-prame-our-services " src="<?php echo base_url('assets/image/IoT website-01.png');?>">
          </div>
        </div>
        <div class="col-md-6 pos-text-desc-2 col-md-6">
        <div class="title-2-pos">
          <h4>Internet of Things <br> Development</h4>
        </div>
        <div class="pos-text-2">
          <p>
            Website menjadi sebuah keharusan bagi setiap pelaku bisnis sekarang ini. Melalui website customer anda akan dengan mudah menemukan apa yang mereka butuhkan, memberikan pertimbangan, mempersuasi, hingga bertransaksi. Jangan anggap sepele sebuah website. Website yang baik akan memeberikan dampak yang optimal dan memberikan kepercayaan. Kami berkomitmen untuk memberikan pelayanan terbaik untuk etalase digital Anda. Tampilan yang cantik, fungsionalitas tinggi dan dengan performa yang paling optimal. Nikmati juga maintenance dari kami selamanya.
          </p>
        </div>
        <div class="pos-button-all-suctom">
          <a class="btn-all-custom" href="<?php echo base_url('why_iot');?>">View More <span class="fa fa-chevron-right"></span></a>
        </div>
      </div>
        </div>

        <div class="col-md-12 pos-web-dev">
          <div class="content-left-services col-md-6">
          <div class="pos-screen">
            <img class="size-img-prame-our-services " src="<?php echo base_url('assets/image/web-dev-01.png');?>">
          </div>
        </div>
          <div class="pos-text-desc-2 col-md-6">
          <div class="title-2-pos">
            <h4>Website <br> Development</h4>
          </div>
          <div class="pos-text-2">
            <p>
              Website menjadi sebuah keharusan bagi setiap pelaku bisnis sekarang ini. Melalui website customer anda akan dengan mudah menemukan apa yang mereka butuhkan, memberikan pertimbangan, mempersuasi, hingga bertransaksi. Jangan anggap sepele sebuah website. Website yang baik akan memeberikan dampak yang optimal dan memberikan kepercayaan. Kami berkomitmen untuk memberikan pelayanan terbaik untuk etalase digital Anda. Tampilan yang cantik, fungsionalitas tinggi dan dengan performa yang paling optimal. Nikmati juga maintenance dari kami selamanya.
            </p>
          </div>
          <div class="pos-button-all-suctom">
            <a class="btn-all-custom" href="<?php echo base_url('why_website');?>">View More <span class="fa fa-chevron-right"></span></a>
          </div>
        </div>
      </div>
      </div>
    </section>

    <section class="container" id="our-services">
      <div class="row">
        <div class="col-md-12 pos-web-dev">
        <div class="col-md-6">
          <div class="pos-screen">
              <div id="scren-lyer-con">
                  <img src="<?php echo base_url('assets/image/mob-dev-01.png');?>">

                  <!-- <div id="img11" class="img-lyr-41">
                      <img src="<?php echo base_url('assets/image/banner/WSP-Web-Images-04.png');?>">
                  </div>
                  <div id="img21" class="img-lyr-31">
                      <img src="<?php echo base_url('assets/image/banner/WSP-Web-Images-03.png');?>">
                  </div>
                  <div id="img31" class="img-lyr-21">
                      <img src="<?php echo base_url('assets/image/banner/WSP-Web-Images-02.png');?>">
                  </div>
                  <div id="img41" class="img-lyr-11">
                      <img src="<?php echo base_url('assets/image/banner/WSP-web-dev-01.png');?>">
                  </div> -->
              </div>
          </div>
        </div>
          <div class="content-left-services col-md-6 pos-text-desc-2">
            <div class="title-2-pos">
              <h4>Mobile Apps <br> Development</h4>
            </div>
            <div class="pos-text-2">
              <p>
                Miliki relasi tanpa batas ruang dan waktu dengan customer Anda. Hal itu hanya bisa dilakukan apabila Anda memiliki sebuah mobile apps yang selalu dikantongi oleh customer Anda. Berikan solusi digital yang terintegrasi bagi mereka, biarkan mereka mendapatkan penggalaman seutuhnya dari brand Anda, berikan perhatian-perhatian kecil melalui push notofikasi yang hanya bisa dilakukan pada mobile apps.
              </p>
            </div>
            <div class="pos-button-all-suctom">
              <a class="btn-all-custom" href="<?php echo base_url('why_mobile');?>">View More <span class="fa fa-chevron-right"></span></a>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="container" id="Enterprise-services">
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-6 content-left-services">
            <div class="pos-screen">
              <img class="size-img-prame-our-services " src="<?php echo base_url('assets/image/ess-01.png');?>">
            </div>
          </div>
          <div class="col-md-6 pos-text-desc-2">
            <div class="title-2-pos">
              <h4>Enterprise <br> Solution System</h4>
            </div>
            <div class="pos-text-2">
              <p>
                Solusi bagi semua keperluan komputasi yang sistematis untuk bisnis Anda. Tingkatkan performa dan efisiensi bisnis melalui ESS dari kami . Miliki lah system yang saling terintegrasi, handal dan real time untuk setiap kegiatan perusahaan Anda. Dari mulai sistem POS, Absensi, Project Task Management, hingga inventori serahkan kepada kami dan Anda dapat fokus kepada kegiatan bisnis utama Anda. Track semua kegiatan bisnis Anda dalam satu platform real time dengan kecepatan maksimal.
              </p>
            </div>
            <div class="pos-button-all-suctom">
              <a class="btn-all-custom" href="#">View More <span class="fa fa-chevron-right"></span></a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--close content our services-->

    <!--open content why choose us-->
    <section class="container" id="why-choose-us">
      <div class="row">
        <div class="col-md-12 po-1200-size">
          <div class="title-1-pos-2">
            <h2 class="title-for-pc">Why Choose Us</h2>
            <h3 class="title-for-mobile">Why Choose Use</h3>
          </div>
          <div class="pos-cont-why-choose-us">
          <div class="pos-cont-why-choose-us-01">
            <div class="content-col3">
              <div class="head">
                <span>01</span>
              </div>
              <div class="body">
                <h3>Text header 1</h3>
              </div>
              <div class="foot">
                <p class="text-desc-choose">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
              </div>
            </div>
          </div>
          <div class="pos-cont-why-choose-us-01">
            <div class="content-col3">
              <div class="head">
                <span>02</span>
              </div>
              <div class="body">
                <h3>Text header 2</h3>
              </div>
              <div class="foot">
                <p class="text-desc-choose">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
              </div>
            </div>
          </div>

          <div class="pos-cont-why-choose-us-01">
            <div class="content-col3">
              <div class="head">
                <span>03</span>
              </div>
              <div class="body">
                <h3>Text header 3</h3>
              </div>
              <div class="foot">
                <p class="text-desc-choose">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
              </div>
            </div>
          </div>

          <div class="pos-cont-why-choose-us-01">
            <div class="content-col3">
              <div class="head">
                <span>04</span>
              </div>
              <div class="body">
                <h3>Text header 4</h3>
              </div>
              <div class="foot">
                <p class="text-desc-choose">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
              </div>
            </div>
          </div>
          <div class="pos-cont-why-choose-us-01">
            <div class="content-col3">
              <div class="head">
                <span>05</span>
              </div>
              <div class="body">
                <h3>Text header 5</h3>
              </div>
              <div class="foot">
                <p class="text-desc-choose">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
              </div>
            </div>
          </div>

          <div class="pos-cont-why-choose-us-01">
            <div class="content-col3">
              <div class="head">
                <span>06</span>
              </div>
              <div class="body">
                <h3>Text header 6</h3>
              </div>
              <div class="foot">
                <p class="text-desc-choose">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>
    </section>
    <!--close content why choose us-->

    <!--open content our work-->
    <section class="container" class="" id="our-work">
      <div class="row">
        <div class="col-md-12 po-1200-size">
          <div class="title-1-pos-2 title-our-works">
            <h2 class="title-for-pc1">Our Works</h2>
            <h3 class="title-for-mobile">Our Works</h3>
          </div>
          <div id="myCarousel" class="carousel slide pos-caro-home" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
              <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
              <li data-target="#myCarousel" data-slide-to="1"></li>
              <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
              <div class="item active">
                <div class="content-crousel">
                    <div class="left">
                      <img class="size-img-prame-our-services for-mob-im" src="<?php echo base_url('assets/image/Mob-aps-works-01.png');?>">
                    </div>
                </div>
                <div class="content-crousel">
                      <div class="right">
                        <div class="title-2-pos">
                          <h4>Website <br> Development</h4>
                        </div>
                        <div class="pos-text-2">
                          <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                          </p>
                        </div>
                        <div class="pos-button-all-suctom">
                          <a class="btn-all-custom" href="jejaring-apps.html">View More <span class="fa fa-chevron-right"></span></a>
                        </div>
                      </div>
                  </div>
              </div>

              <div class="item">
                <div class="content-crousel">
                    <div class="left">
                    <img class="size-img-prame-our-services " src="<?php echo base_url('assets/image/motzint-web-01.png');?>">
                    </div>
                </div>
                <div class="content-crousel">
                      <div class="right">
                        <div class="title-2-pos">
                          <h4>Website <br> Development</h4>
                        </div>
                        <div class="pos-text-2">
                          <p >
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                          </p>
                        </div>
                        <div class="pos-button-all-suctom">
                          <a class="btn-all-custom" href="jejaring-apps.html">View More <span class="fa fa-chevron-right"></span></a>
                        </div>
                      </div>
                  </div>
              </div>

              <div class="item">
                <div class="content-crousel">
                    <div class="left">
                      <img class="size-img-prame-our-services " src="<?php echo base_url('assets/image/Our-works-1-01.png');?>">
                    </div>
                </div>
                <div class="content-crousel">
                      <div class="right">
                        <div class="title-2-pos">
                          <h4>Website <br> Development</h4>
                        </div>
                        <div class="pos-text-2">
                          <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                          </p>
                        </div>
                        <div class="pos-button-all-suctom">
                          <a class="btn-all-custom" href="jejaring-apps.html">View More <span class="fa fa-chevron-right"></span></a>
                        </div>
                      </div>
                  </div>
              </div>

            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
              <span class="fa fa-chevron-left"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
              <span class="fa fa-chevron-right"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
    </section>
    <!--close content our work-->

    <section class="container footer-content">
      <div class="row">
        <div class="col-md-12 po-1200-size">
          <div id="content-footer">
          <div class="foot-header">
              <h3>Hello WSP!</h3>
          </div>
          <div class="foot-body col-md-12">
              <div class="left col-md-6">
                  <div class="foot-title">
                      <h4><a href="mailto:hello@wesolveproblems.id?Subject=hello"><span class="fa fa-envelope-o"></span><i  class="gradien-color-title-font">hello@wesolveproblems.id</i></a></h4>
                  </div>
                  <div class="body">
                      <ul>
                          <li><a href="https://bit.ly/2D9IbIv" target="_blank"><span class="fa fa-map-marker"></span> Perumahan Serpong Garden 2, Cluster Greenland,  Blok A13/5, Cisauk,  Tangerang, Banten, Indonesia</a></li>
                          <li><a href="tel:622175682152"><span class="fa fa-phone"></span>+6221 756 82 152</a></li>
                      </ul>
                  </div>
              </div>
              <div class="right col-md-6">
                  <div class="post-form">
                      <form id="" method="" action="">
                          <div class="form-group">
                              <label>Name</label>
                              <input class="text-input" type="text" name="name" id="name">
                          </div>
                          <div class="form-group">
                              <label>Email</label>
                              <input class="text-input" type="text" name="email" id="email">
                          </div>
                          <div class="form-group">
                              <label>Message</label>
                              <textarea class="text-input" id="message" name="message" rows="3"></textarea>
                          </div>
                          <div class="btn-submit-pos">
                              <button type="submit" class="btn-submit">Send<span class="fa fa-chevron-right"></span></button>
                          </div>
                      </form>
                  </div>
              </div>
          </div></div>
        </div>
      </div>
    </section>
    <footer>
      <div class="con-foot-01">
        <div class="right-cont-foot">
          <ul>
            <li>
              <a href="#">
                <img class="logo-botom"src="<?php echo base_url('assets/image/banner/logo-putih-01.png');?>">
              </a>
            </li>
          </ul>
        </div>
        <div class="center-cont-foot">
          <ul>
            <li>
              <a href="#">About Us</a>
            </li>
            <li>
              <a href="#">Our Services</a>
            </li>
            <li>
              <a href="#">Our Clients</a>
            </li>
            <li>
              <a href="#">Our Projects</a>
            </li>
          </ul>
        </div>
        <div class="left-cont-foot">
          <div class="title-foot">
            <h5>Our Channel</h5>
          </div>
          <ul>
            <li>
              <a href="#"><span class="fa fa-linkedin"></span></a>
            </li>
            <li>
              <a href="#"><span class="fa fa-whatsapp"></span></a>
            </li>
            <li>
              <a href="#"><span class="fa fa-instagram"></span></a>
            </li>
            <li>
              <a href="#"><span class="fa fa-facebook"></span></a>
            </li>
          </ul>
        </div>
      </div>
     </footer>

  </div>
</div>
<!--pen modal menus-->
    <div class="modal right fade" id="ModalClintArea" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel2">Client Area</h4>
          </div>

          <div class="modal-body">
              <form id="" action="" method="">
                    <div class="form-group">
                        <input type="text" class="all-text-input-style" name="emil" id="email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <input type="password" class="all-text-input-style" name="passwprd" id="password" placeholder="password">
                    </div>
                    <div class="pos-btn-login">
                      <div class="btn-left-log">
                          <button type="submit">login <span class="fa fa-chevron-right"></span></button>
                      </div>
                      <div class="btn-right-log">
                          <a href="#" class="">Registrasi</a>
                      </div>
                    </div>
              </form>
          </div>

        </div><!-- modal-content -->
      </div><!-- modal-dialog -->
    </div>
<!-- modal -->

<!--pen modal menus-->
<div class="modal right fade" id="ModalWorkingArea" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
    <div class="modal-dialog" role="document">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel2">Working Area</h4>
        </div>

        <div class="modal-body">
            <form id="" action="" method="">
                <div class="form-group">
                    <input type="text" class="all-text-input-style" name="emil" id="email" placeholder="Email">
                </div>
                <div class="form-group">
                    <input type="password" class="all-text-input-style" name="passwprd" id="password" placeholder="password">
                </div>
                <div class="pos-btn-login">
                  <div class="btn-left-log">
                      <button type="submit">login <span class="fa fa-chevron-right"></span></button>
                  </div>
                  <div class="btn-right-log">
                      <a href="#" class="">Registrasi</a>
                  </div>
                </div>
            </form>
        </div>

      </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div>
<!-- modal -->
<script>
$(document).ready(function(){
  $(window).scroll(function(){
    var scroll = $(window).scrollTop();
    if (scroll > 200) {
          $("#menus-nav").removeClass("background-transparent");
          $("#menus-nav").addClass("background-white");
          $("#menu-text-a").removeClass("color-white");
          $("#menu-text-a2").removeClass("color-white");
          $("#menu-text-a3").removeClass("color-white");
          $("#menu-text-a").addClass("color-black");
          $("#menu-text-a2").addClass("color-black");
          $("#menu-text-a3").addClass("color-black");
          $("#float-btn").removeClass("float-bnt-custom-color-white");
          $("#float-btn").addClass("float-bnt-custom-color-black");
          $("#float-btn2").removeClass("float-bnt-custom-color-white");
          $("#float-btn2").addClass("float-bnt-custom-color-black");
          $("#ciscle-01").removeClass("cr-color-white");
          $("#ciscle-01").addClass("cr-color-black");
          $("#ciscle-02").removeClass("cr-color-white");
          $("#ciscle-02").addClass("cr-color-black");
          $('#scroll').fadeIn();
    }

    else{
          $("#menus-nav").removeClass("background-white");
          $("#menus-nav").addClass("background-transparent");
          $("#menu-text-a").removeClass("color-black");
          $("#menu-text-a2").removeClass("color-black");
          $("#menu-text-a3").removeClass("color-black");
          $("#menu-text-a").addClass("color-white");
          $("#menu-text-a2").addClass("color-white");
          $("#menu-text-a3").addClass("color-white");
          $("#float-btn").removeClass("float-bnt-custom-color-black");
          $("#float-btn").addClass("float-bnt-custom-color-white");
          $("#float-btn2").removeClass("float-bnt-custom-color-black");
          $("#float-btn2").addClass("float-bnt-custom-color-white");
          $("#ciscle-01").removeClass("cr-color-black");
          $("#ciscle-01").addClass("cr-color-white");
          $("#ciscle-02").removeClass("cr-color-black");
          $("#ciscle-02").addClass("cr-color-white");
          $('#scroll').fadeOut();
    }
  });
  $('#scroll').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });
})
$("a[href^='#our-services']").click(function() {
  $('html, body').animate({
      scrollTop: $("#our-services").offset().top
  }, 1800);
});
$("a[href^='#our-work']").click(function() {
  $('html, body').animate({
      scrollTop: $("#our-work").offset().top
  }, 1800);
});
</script>
