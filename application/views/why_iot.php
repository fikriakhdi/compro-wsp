<head>
  <style>
    .navbar-default .navbar-nav>li>a {
      color: #000;
    }
    .float-bnt-custom-color-white {
        color: #000;
    }
    .pos-float-btn ul li a {
      color:#000;
    }
    .cr-color-white {
      color:#000!important;
      border: 1px solid #000!important;
    }
  </style>
</head>
<a href="#" id="scroll" style="display: none;"><span></span></a>
<div id="content-web">

  <section class="container" id="why-website-app">
    <div class="row">
      <div class="col-md-12">
        <div class="title-1-pos col-md-12">
          <div class="col-md-6">
          </div>
          <div class="col-md-6 right-title">
            <h3>Why IoT?</h3>
          </div>
        </div>
        <div class="col-md-6 content-left-services">
          <div class="pos-screen">
            <img class="size-img-prame-our-services " src="<?php echo base_url('assets/image/IoT website-01.png');?>">
          </div>
        </div>
        <div class="col-md-6 pos-text-desc-2">
          <div class="title-2-pos">
            <h4>Bring Conventional<br>Network Marketing<br>Bussiness In Digital Way</h4>
          </div>
          <div class="pos-text-2">
            <p>
              Solusi bagi semua keperluan komputasi yang sistematis untuk bisnis Anda. Tingkatkan performa dan efisiensi bisnis melalui ESS dari kami . Miliki lah system yang saling terintegrasi, handal dan real time untuk setiap kegiatan perusahaan Anda. Dari mulai sistem POS, Absensi, Project Task Management, hingga inventori serahkan kepada kami dan Anda dapat fokus kepada kegiatan bisnis utama Anda. Track semua kegiatan bisnis Anda dalam satu platform real time dengan kecepatan maksimal.
            </p>
          </div>
          <div class="pos-button-all-suctom">
            <a class="btn-all-custom" href="#">View More <span class="fa fa-chevron-right"></span></a>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="container" id="pos-search">
    <div class="row">
      <div class="col-md-12 po-1200-size">
        <div class="title-second">
            <h3>Your Costumers Search<br>Through Google</h3>
        </div>
        <div class="col-md-12 pos-cont-search">
            <div class="col-md-6 left">
                <p>Lorem Ipsum Dolor Sit Amet consecte lobare ne tukale bsdkoaleyb skb jh pnelndfhu sa,huebsbnx, shjiku sjjbva msbjhgdv lasb Lorem Ipsum Dolor Sit Amet consecte lobare ne tukaleLorem Ipsum Dolor Sit Amet consecte lobare ne tukale Lorem Ipsum Dolor Sit Amet consecte lobare ne tukale Lorem Ipsum Dolor Sit Amet consecte lobare ne tukale</p>
            </div>
            <div class="col-md-6 right">
                <div class="pos-icon-google-search">
                    <img src="<?php echo base_url();?>assets/image/banner/google_icon.png" class>
                </div>
                <div class="pos-form-search">
                    <form id="search-form-input" metod="" action="">
                        <div class="form-group">
                            <input class="form-custpm-seach-google" type="text" name="search_google" id="search_google">
                            <span class="fa fa-microphone"></span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      </div>
    </div>
  </section>

  <!--open content why choose us-->
  <section class="container" id="why-choose-us">
    <div class="row">
      <div class="col-md-12 po-1200-size">
        <div class="title-1-pos-all-you-need">
          <h3>What Website Do We Create?</h3>
        </div>
        <div class="pos-cont-why-choose-us">
        <div class="pos-cont-why-choose-us-01">
          <div class="content-col3">
            <div class="head">
              <span>01</span>
            </div>
            <div class="body">
              <h3>Text header 1</h3>
            </div>
            <div class="foot">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
            </div>
          </div>
        </div>
        <div class="pos-cont-why-choose-us-01">
          <div class="content-col3">
            <div class="head">
              <span>02</span>
            </div>
            <div class="body">
              <h3>Text header 2</h3>
            </div>
            <div class="foot">
               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
            </div>
          </div>
        </div>

        <div class="pos-cont-why-choose-us-01">
          <div class="content-col3">
            <div class="head">
              <span>03</span>
            </div>
            <div class="body">
              <h3>Text header 3</h3>
            </div>
            <div class="foot">
               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
            </div>
          </div>
        </div>

        <div class="pos-cont-why-choose-us-01">
          <div class="content-col3">
            <div class="head">
              <span>04</span>
            </div>
            <div class="body">
              <h3>Text header 4</h3>
            </div>
            <div class="foot">
               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
            </div>
          </div>
        </div>
        <div class="pos-cont-why-choose-us-01">
          <div class="content-col3">
            <div class="head">
              <span>05</span>
            </div>
            <div class="body">
              <h3>Text header 5</h3>
            </div>
            <div class="foot">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
            </div>
          </div>
        </div>

        <div class="pos-cont-why-choose-us-01">
          <div class="content-col3">
            <div class="head">
              <span>06</span>
            </div>
            <div class="body">
              <h3>Text header 6</h3>
            </div>
            <div class="foot">
               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
  </section>
  <!--close content why choose us-->

  <section class="container" id="pos-all-need">
    <div class="row">
      <div class="title-1-pos-all-you-need">
          <h3>"All You Need Is A Good Website"</h3>
      </div>
      <div class="des">
          <p>- We Solve Problem -</p>
      </div>
    </div>
  </section>
  <section class="container footer-content">
    <div class="row">
      <div class="col-md-12 po-1200-size">
        <div id="content-footer">
        <div class="foot-header">
            <h3>Hello WSP!</h3>
        </div>
        <div class="foot-body col-md-12">
            <div class="left col-md-6">
                <div class="foot-title">
                    <h4><a href="mailto:hello@wesolveproblems.id?Subject=hello"><span class="fa fa-envelope-o"></span><i  class="gradien-color-title-font">hello@wesolveproblems.id</i></a></h4>
                </div>
                <div class="body">
                    <ul>
                        <li><a href="https://bit.ly/2D9IbIv" target="_blank"><span class="fa fa-map-marker"></span> Perumahan Serpong Garden 2, Cluster Greenland,  Blok A13/5, Cisauk,  Tangerang, Banten, Indonesia</a></li>
                        <li><a href="tel:622175682152"><span class="fa fa-phone"></span>+6221 756 82 152</a></li>
                    </ul>
                </div>
            </div>
            <div class="right col-md-6">
                <div class="post-form">
                    <form id="" method="" action="">
                        <div class="form-group">
                            <label>Name</label>
                            <input class="text-input" type="text" name="name" id="name">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input class="text-input" type="text" name="email" id="email">
                        </div>
                        <div class="form-group">
                            <label>Message</label>
                            <textarea class="text-input" id="message" name="message" rows="3"></textarea>
                        </div>
                        <div class="btn-submit-pos">
                            <button type="submit" class="btn-submit">Send<span class="fa fa-chevron-right"></span></button>
                        </div>
                    </form>
                </div>
            </div>
        </div></div>
      </div>
    </div>
  </section>
  <footer>
    <div class="con-foot-01">
      <div class="right-cont-foot">
        <ul>
          <li>
            <a href="#">
              <img class="logo-botom"src="<?php echo base_url('assets/image/banner/logo-putih-01.png');?>">
            </a>
          </li>
        </ul>
      </div>
      <div class="center-cont-foot">
        <ul>
          <li>
            <a href="#">About Us</a>
          </li>
          <li>
            <a href="#">Our Services</a>
          </li>
          <li>
            <a href="#">Our Clients</a>
          </li>
          <li>
            <a href="#">Our Projects</a>
          </li>
        </ul>
      </div>
      <div class="left-cont-foot">
        <div class="title-foot">
          <h5>Our Channel</h5>
        </div>
        <ul>
          <li>
            <a href="#"><span class="fa fa-linkedin"></span></a>
          </li>
          <li>
            <a href="#"><span class="fa fa-whatsapp"></span></a>
          </li>
          <li>
            <a href="#"><span class="fa fa-instagram"></span></a>
          </li>
          <li>
            <a href="#"><span class="fa fa-facebook"></span></a>
          </li>
        </ul>
      </div>
    </div>
   </footer>
</div>


<div class="window" id="content-mobile">
  <div class="header">
    <div class="burger-container">
      <div id="burger">
        <div class="bar topBar"></div>
        <div class="bar btmBar"></div>
      </div>
    </div>
    <div class="icon icon-apple"></div>
    <div class="pos-icon-nav"><a href="<?php echo base_url('home');?>"><img src="<?php echo base_url('assets/image/banner/logo-putih-01.png');?>"></a></div>
    <ul class="menu">
      <li class="menu-item"><a href="#">Our Services</a></li>
      <li class="menu-item"><a href="#">Our Works</a></li>
      <li class="menu-item"><a href="#">Client Area</a></li>
      <li class="menu-item"><a href="#">Working Area</a></li>
    </ul>
    <!-- <div class="shop icon icon-bag"></div> -->
  </div>
  <div class="content">
    <section class="container" id="why-website-app">
      <div class="row">
        <div class="col-md-12">
          <div class="title-1-pos col-md-12">
            <div class="col-md-6">
            </div>
            <div class="col-md-6 right-title">
              <h3>Why IoT?</h3>
            </div>
          </div>
          <div class="col-md-6 content-left-services">
            <div class="pos-screen">
              <img class="size-img-prame-our-services " src="<?php echo base_url('assets/image/IoT website-01.png');?>">
            </div>
          </div>
          <div class="col-md-6 pos-text-desc-2">
            <div class="title-2-pos">
              <h4>Bring Conventional<br>Network Marketing<br>Bussiness In Digital Way</h4>
            </div>
            <div class="pos-text-2">
              <p>
                Solusi bagi semua keperluan komputasi yang sistematis untuk bisnis Anda. Tingkatkan performa dan efisiensi bisnis melalui ESS dari kami . Miliki lah system yang saling terintegrasi, handal dan real time untuk setiap kegiatan perusahaan Anda. Dari mulai sistem POS, Absensi, Project Task Management, hingga inventori serahkan kepada kami dan Anda dapat fokus kepada kegiatan bisnis utama Anda. Track semua kegiatan bisnis Anda dalam satu platform real time dengan kecepatan maksimal.
              </p>
            </div>
            <div class="pos-button-all-suctom">
              <a class="btn-all-custom" href="#">View More <span class="fa fa-chevron-right"></span></a>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="container" id="pos-search">
      <div class="row">
        <div class="col-md-12 po-1200-size">
          <div class="col-md-12 pos-cont-search">
            <div class="col-md-6 right">
                <div class="pos-icon-google-search">
                    <img src="<?php echo base_url();?>assets/image/banner/google_icon.png" class>
                </div>
                <div class="pos-form-search">
                    <form id="search-form-input" metod="" action="">
                        <div class="form-group">
                            <input class="form-custpm-seach-google" type="text" name="search_google" id="search_google">
                            <span class="fa fa-microphone"></span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="title-second">
                <h3>Your Costumers Search<br>Through Google</h3>
            </div>
            <div class="col-md-6 left">
                  <p>Lorem Ipsum Dolor Sit Amet consecte lobare ne tukale bsdkoaleyb skb jh pnelndfhu sa,huebsbnx, shjiku sjjbva msbjhgdv lasb Lorem Ipsum Dolor Sit Amet consecte lobare ne tukaleLorem Ipsum Dolor Sit Amet consecte lobare ne tukale Lorem Ipsum Dolor Sit Amet consecte lobare ne tukale Lorem Ipsum Dolor Sit Amet consecte lobare ne tukale</p>
              </div>
          </div>
        </div>
      </div>
    </section>

    <!--open content why choose us-->
    <section class="container" id="why-choose-us">
      <div class="row">
        <div class="col-md-12 po-1200-size">
          <div class="title-1-pos-all-you-need">
            <h3>What Website Do We Create?</h3>
          </div>
          <div class="pos-cont-why-choose-us">
          <div class="pos-cont-why-choose-us-01">
            <div class="content-col3">
              <div class="head">
                <span>01</span>
              </div>
              <div class="body">
                <h3>Text header 1</h3>
              </div>
              <div class="foot">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
              </div>
            </div>
          </div>
          <div class="pos-cont-why-choose-us-01">
            <div class="content-col3">
              <div class="head">
                <span>02</span>
              </div>
              <div class="body">
                <h3>Text header 2</h3>
              </div>
              <div class="foot">
                 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
              </div>
            </div>
          </div>

          <div class="pos-cont-why-choose-us-01">
            <div class="content-col3">
              <div class="head">
                <span>03</span>
              </div>
              <div class="body">
                <h3>Text header 3</h3>
              </div>
              <div class="foot">
                 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
              </div>
            </div>
          </div>

          <div class="pos-cont-why-choose-us-01">
            <div class="content-col3">
              <div class="head">
                <span>04</span>
              </div>
              <div class="body">
                <h3>Text header 4</h3>
              </div>
              <div class="foot">
                 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
              </div>
            </div>
          </div>
          <div class="pos-cont-why-choose-us-01">
            <div class="content-col3">
              <div class="head">
                <span>05</span>
              </div>
              <div class="body">
                <h3>Text header 5</h3>
              </div>
              <div class="foot">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
              </div>
            </div>
          </div>

          <div class="pos-cont-why-choose-us-01">
            <div class="content-col3">
              <div class="head">
                <span>06</span>
              </div>
              <div class="body">
                <h3>Text header 6</h3>
              </div>
              <div class="foot">
                 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>
    </section>
    <!--close content why choose us-->

    <section class="container" id="pos-all-need">
      <div class="row">
        <div class="title-1-pos-all-you-need">
            <h3>"All You Need Is A Good Website"</h3>
        </div>
        <div class="des">
            <p>- We Solve Problem -</p>
        </div>
      </div>
    </section>
    <section class="container footer-content">
      <div class="row">
        <div class="col-md-12 po-1200-size">
          <div id="content-footer">
          <div class="foot-header">
              <h3>Hello WSP!</h3>
          </div>
          <div class="foot-body col-md-12">
              <div class="left col-md-6">
                  <div class="foot-title">
                      <h4><a href="mailto:hello@wesolveproblems.id?Subject=hello"><span class="fa fa-envelope-o"></span><i  class="gradien-color-title-font">hello@wesolveproblems.id</i></a></h4>
                  </div>
                  <div class="body">
                      <ul>
                          <li><a href="https://bit.ly/2D9IbIv" target="_blank"><span class="fa fa-map-marker"></span> Perumahan Serpong Garden 2, Cluster Greenland,  Blok A13/5, Cisauk,  Tangerang, Banten, Indonesia</a></li>
                          <li><a href="tel:622175682152"><span class="fa fa-phone"></span>+6221 756 82 152</a></li>
                      </ul>
                  </div>
              </div>
              <div class="right col-md-6">
                  <div class="post-form">
                      <form id="" method="" action="">
                          <div class="form-group">
                              <label>Name</label>
                              <input class="text-input" type="text" name="name" id="name">
                          </div>
                          <div class="form-group">
                              <label>Email</label>
                              <input class="text-input" type="text" name="email" id="email">
                          </div>
                          <div class="form-group">
                              <label>Message</label>
                              <textarea class="text-input" id="message" name="message" rows="3"></textarea>
                          </div>
                          <div class="btn-submit-pos">
                              <button type="submit" class="btn-submit">Send<span class="fa fa-chevron-right"></span></button>
                          </div>
                      </form>
                  </div>
              </div>
          </div></div>
        </div>
      </div>
    </section>
    <footer>
      <div class="con-foot-01">
        <div class="right-cont-foot">
          <ul>
            <li>
              <a href="#">
                <img class="logo-botom"src="<?php echo base_url('assets/image/banner/logo-putih-01.png');?>">
              </a>
            </li>
          </ul>
        </div>
        <div class="center-cont-foot">
          <ul>
            <li>
              <a href="#">About Us</a>
            </li>
            <li>
              <a href="#">Our Services</a>
            </li>
            <li>
              <a href="#">Our Clients</a>
            </li>
            <li>
              <a href="#">Our Projects</a>
            </li>
          </ul>
        </div>
        <div class="left-cont-foot">
          <div class="title-foot">
            <h5>Our Channel</h5>
          </div>
          <ul>
            <li>
              <a href="#"><span class="fa fa-linkedin"></span></a>
            </li>
            <li>
              <a href="#"><span class="fa fa-whatsapp"></span></a>
            </li>
            <li>
              <a href="#"><span class="fa fa-instagram"></span></a>
            </li>
            <li>
              <a href="#"><span class="fa fa-facebook"></span></a>
            </li>
          </ul>
        </div>
      </div>
     </footer>
  </div>
</div>

<script>
$(document).ready(function(){
  $(window).scroll(function(){
    var scroll = $(window).scrollTop();
    if (scroll > 200) {
          $('#scroll').fadeIn();
    }

    else{
          $('#scroll').fadeOut();
    }
  });
  $('#scroll').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });
})
//
// $("a[href^='#']").click(function(e) {
// 	e.preventDefault();
//
// 	var position = $($(this).attr("href")).offset().top;
//
// 	$("body, html").animate({
// 		scrollTop: position
// 	} /* speed */ );
//
//
// });
$("a[href^='#our-services']").click(function() {
  $('html, body').animate({
      scrollTop: $("#our-services").offset().top
  }, 1800);
});
$("a[href^='#our-work']").click(function() {
  $('html, body').animate({
      scrollTop: $("#our-work").offset().top
  }, 1800);
});
</script>
