$(document).ready(function(){
  $(window).scroll(function(){
  	var scroll = $(window).scrollTop();
	  if (scroll > 200) {
          $("#menus-nav").removeClass("background-transparent");
          $("#menus-nav").addClass("background-white");
          $("#menu-text-a").addClass("color-black");
          $("#menu-text-a2").addClass("color-black");
          $("#menu-text-a3").addClass("color-black");
          // $("#menu-text-a").removeClass("color-white");
          // $("#menu-text-a2").removeClass("color-white");
          // $("#menu-text-a3").removeClass("color-white");
          // $("#menu-text-a").addClass("color-black");
          // $("#menu-text-a2").addClass("color-black");
          // $("#menu-text-a3").addClass("color-black");
          $("#float-btn").removeClass("float-bnt-custom-color-white");
          $("#float-btn").addClass("float-bnt-custom-color-black");
          $("#float-btn2").removeClass("float-bnt-custom-color-white");
          $("#float-btn2").addClass("float-bnt-custom-color-black");
          // $("#ciscle-01").removeClass("cr-color-white");
          // $("#ciscle-01").addClass("cr-color-black");
          // $("#ciscle-02").removeClass("cr-color-white");
          // $("#ciscle-02").addClass("cr-color-black");
	  }

	  else{
          $("#menus-nav").removeClass("background-white");
          $("#menus-nav").addClass("background-transparent");
          $("#menu-text-a").addClass("color-black");
          $("#menu-text-a2").addClass("color-black");
          $("#menu-text-a3").addClass("color-black");
          // $("#menu-text-a").addClass("color-white");
          // $("#menu-text-a2").addClass("color-white");
          // $("#menu-text-a3").addClass("color-white");
          $("#float-btn").removeClass("float-bnt-custom-color-black");
          $("#float-btn").addClass("float-bnt-custom-color-white");
          $("#float-btn2").removeClass("float-bnt-custom-color-black");
          $("#float-btn2").addClass("float-bnt-custom-color-white");
          // $("#ciscle-01").removeClass("cr-color-black");
          // $("#ciscle-01").addClass("cr-color-white");
          // $("#ciscle-02").removeClass("cr-color-black");
          // $("#ciscle-02").addClass("cr-color-white");
	  }
  });
  (function() {
    var burger = document.querySelector(".burger-container"),
        header = document.querySelector(".header");

    burger.onclick = function() {
        header.classList.toggle("menu-opened");
    };
})();
})
